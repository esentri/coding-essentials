---
size: 1080p
#canvas: "#32373c"
transition: crossfade 0.2
#background: music/background.mp3 0.05 fade-out fade-in
#theme: default
theme: custom.css
subtitles: embed
voice: Ilse

---

Hallo und herzlich Willkommen bei den "esenntrie coding essentials". Diesmal geht es um die Tücken der Vererbung.

(pause: 1)


![contain](slides/04_tuecken_der_vererbung/titel.png)
---
In vielen Tutorials, Lehrveranstaltungen und Büchern wird Vererbung als eines der zentralen Werkzeuge der Objektorientierung angepriesen.

(pause: 1)

Allerdings bringt die Vererbung einige Probleme mit sich, die selten direkt erwähnt werden.

```md
## Wie? Wo? Was? Warum?
```

---

Eines der Probleme ist, dass die Kapselung einer Klasse aufgehoben wird. Die Kindklasse muss sich auf die Implementierung der Elternklasse verlassen. Falls sich die Implementierung der Elternklasse ändert, kann dies dazu führen, dass die Kindklasse nicht mehr korrekt arbeitet.

(pause: 1)

```md
## Kapselung?
- wird gebrochen
  - Kind hängt von interna der Elternklasse ab
```

---

Angenommen wir möchten die Anzahl der eingefügten Elemente in einem *[HashSet]{en}* zählen. Eine Implementierung könnte so aussehen.
*[HashSet]{en}*  wird extended und wir überschreiben *[add]{en}*  und *[addAll]{en}* um beim Einfügen die Anzahl der hinzugefügten Elemente zu zählen.
Diese Implementierung ist sehr stark an die Elternklasse gekoppelt und damit auch von deren Implementierung abhängig.

(pause: 1)

Obwohl die Implementierung auf den ersten Blick in Ordnung zu sein scheint, hat sie einen *[Bug]{en}* .

(pause: 1)

(font-size: 40)
```java
public class CountingSet<E> extends HashSet<E> {
    private int addCount = 0;

    @Override
    public boolean add(E e) {
        addCount++;
        return super.add(e);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        addCount += c.size();
        return super.addAll(c);
    }

    public int getAddCount() { return addCount; }
}
```

---
Ein Test zeigt, dass Elemente offensichtlich doppelt gezählt werden. Denn anstatt bei 3 steht der Zähler bei 5, obwohl wir nur 3 Elemente hinzugefügt haben.

(pause: 1)

Wie kommt es dazu? Grund dafür ist, dass *[HashSet]{en}* bei einem Aufruf von *[add all]{en}* intern *[add]{en}* aufruft. Selbstverständlich könnten wir uns dieses Wissen zu Nutze machen und die Implementierung von *[CountingSet]{en}* anpassen. Falls sich aber *[HashSet]{en}* in Zukunft ändert und intern nicht mehr *[add]{en}* aufruft, ist das Zählen der Eingefügten Elemente nicht mehr korrekt, obwohl wir nichts an der Implementierung geändert haben.

(pause: 1)

(font-size: 40)
```java
CountingSet<String> set = new CountingSet<>();

set.add("eins");
set.addAll(List.of("zwei", "drei"));

System.out.println(set.getAddCount()); // 5
```
---
Wie kann man dieses Problem vermeiden?
Anstatt von einer Klasse zu erben, gibt es die Möglichkeit diese einzubetten und so eine Komposition herzustellen.

(pause: 1)


```md
## Komposition anstatt Vererbung
```
---

Der größte Vorteil ist, dass man nicht an eine Elternklasse gebunden ist und damit auch einfach die Interna der eigenen Klasse verändern kann. Um das Beispiel von oben aufzugreifen würde das bedeuten, dass man ein *[hashset]{en}* als Klassenvariable erstellt und das Set Interface implementiert. Alle Aufrufe können an das interne *[hashset]{en}* weitergeleitet werden. Falls man weitere Funktionalität hinzufügen möchte, kann man das ohne auf die Implementierung von *[hashset]{en}* achten zu müssen.

(pause: 1)

Das unbequeme an dieser Lösung ist, dass man alle Methoden von Set implementieren muss und an das interne Set weiterreichen.
Glücklicherweise stellt zum Beispiel guava ein *[Forwarding Set]{en}* bereit, welches einem diese Arbeit abnimmt.

(pause: 1)

(font-size: 30)
```java
public class CompositionSet<E> implements Set<E> {

    private HashSet<E> set = new HashSet<>();
    private int addCount = 0;

    @Override
    public boolean add(E e) {
        addCount++;
        return set.add(e);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        addCount += c.size();
        return set.addAll(c);
    }

    @Override public int size() { return set.size(); }
    @Override public boolean isEmpty() { return set.isEmpty(); }
    @Override public boolean contains(Object o) { return set.contains(o); }
    @Override public Iterator<E> iterator() { return set.iterator(); }
    @Override public Object[] toArray() { return set.toArray(); }
    @Override public <T> T[] toArray(T[] a) { return set.toArray(a); }
    @Override public boolean remove(Object o) { return set.remove(o); }
    @Override public boolean containsAll(Collection<?> c) { return set.containsAll(c); }
    @Override public boolean retainAll(Collection<?> c) { return set.retainAll(c); }
    @Override public boolean removeAll(Collection<?> c) { return set.removeAll(c); }
    @Override public void clear() { set.clear(); }
}
```
---

Hier muss nur die *[delegate]{en}* Methode implementiert werden, um das interne Set dem *[Forwarding Set]{en}* zur Verfügung zustellen.
Alle anderen Set Methoden können, müssen aber nicht überschrieben werden.


(font-size: 35)
```java
public class GuavaCompositionSet<E> extends ForwardingSet<E> {

    private HashSet<E> delegate = new HashSet();
    private int addCount = 0;

    @Override
    protected Set<E> delegate() {
        return delegate;
    }

    @Override
    public boolean add(E element) {
        addCount++;
        return super.add(element);
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        addCount += collection.size();
        return super.addAll(collection);
    }

```
---

Mit dieser Implementierung liefert der kleine Test von oben auch das erwartete Ergebnis.
Außerdem sind wir komplett unabhängig von der Set Implementierung und könnten bei Bedarf diese austauschen.

(pause: 1)

(font-size: 40)
```java
GuavaCompositionSet<String> set = new GuavaCompositionSet<>();
set.add("eins");
set.addAll(List.of("zwei", "drei"));

System.out.println(set.getAddCount()); // 3

```
---

Ein weiteren Stolperstein findet man, wenn man Methoden überschreibt, welche im Konstruktor der Elternklasse verwendet werden.

Dies kann zu unerwarteten Effekten führen.

Schauen wir uns hierfür ein Beispiel an.

```md
## Konstruktoren und überschreibbare Methoden
```

---

Angenommen wir haben eine Elternklasse, welche eine überschreibbare Methode in ihrem Konstruktor aufruft.

(pause: 1)

(font-size: 40)
```java
   public class Parent {

	public Parent() {
		overrideMe();
	}

	protected void overrideMe() {
	}
}
```

---

Die Kindklasse implementiert nun die Methode *[overrideMe]{en}* und
im Konstruktor der Kindklasse wird eine Klassenvariable gesetzt.

(pause: 1)

Was geschieht nun wenn man eine Instanz von *[child]{en}* erzeugt?

(pause: 1)

(font-size: 40)
```java
    public final class Child extends Parent {

    private final Instant instant;

	public Child() {
	    instant = Instant.now();
	}

	@Override
	void overrideMe() {
		System.println(instant);
	}
}
```

---

Mit diesem Code erstellen wir ein neues Child-Objekt. Welche Ausgabe wird dieser Code erzeugen?

Ich geb dir etwas Zeit darüber nachzudenken oder du pausierst das Video einfach.


(font-size: 40)
```java
    public static void main(String args[]) {
        Child child = new Child();
        child.overrideMe();
    }
```
---

(pause: 8)

(font-size: 30)
```java
   public class Parent {

	public Parent() {
		overrideMe();
	}

	protected void overrideMe() {
	}
}
//
 public final class Child extends Parent {

    private final Instant instant;

	public Child() {
	    instant = Instant.now();
	}

	@Override
	void overrideMe() {
		System.println(instant);
	}
}
//
    public static void main(String args[]) {
        Child child = new Child();
        child.overrideMe();
    }
```

---

Als ich den Code ausgeführt habe, sah die Ausgabe so aus. 

Es wird *[null]{en}* und das aktuelle Datum ausgegeben. Aber warum?

(pause: 1)



(font-size: 40)
```java
null
2022-05-31T12:05:08.329008Z
```


---

Wenn eine Instanz von *[child]{en}* erzeugt wird, wird auch immer der Superkonstruktor aufgerufen. Dies bedeutet, dass *[overrideMe]{en}* im Konstruktor von *[parent]{en}* ausgeführt wird.
Daher die Ausgabe von *[null]{en}*, da die Klassenvariable *[instant]{en}* noch nicht initialisiert wurde.

(pause: 1)

Danach wird der Konstruktor von *[child]{en}* ausgeführt. Dort wird dann *[instant]{en}* initialisiert. Nun wird noch *[overrideMe]{en}* aufgerufen und damit das zweite *[print line]{en}* aufgerufen.

Wenn wir uns jetzt nochmal die Klasse *[child]{en}* anschauen, fällt auf dass die Variable *[instant]{en}* *[final]{en}* ist, dennoch hat sie in diesem Aufbau zwei verschiedene Werte angenommen, bis die Klasse komplett initialisiert war.

(pause: 1)


(font-size: 40)
```java
   public final class Child extends Parent {

	private final Instant instant;

	Child() {
		instant = Instant.now();
	}

	@Override
	void overrideMe() {
		System.println(instant);
	}
}
```


---


weitere Infos und den Code findet ihr hier.
Vielen Dank für eure Zeit und bis bald!

```md
## weitere Infos
- https://bitbucket.org/esentri/coding-essentials/src/master/source/code/
- Buchempfehlung: Effective Java - Joshua Bloch
```

---
