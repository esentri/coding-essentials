---
size: 1080p
#canvas: "#32373c"
transition: crossfade 0.2
#background: music/background.mp3 0.05 fade-out fade-in
#theme: default
theme: custom.css
subtitles: embed
voice: Ilse

---
![contain](slides/02_hashCode/titel.png)

Hallo und herzlich Willkommen bei den "esenntrie coding essentials". Heute steht die hashCode Methode im Fokus.

(pause: 1)
---
In der Tschawadoc für hashCode in Object ist die Rede von einem *[general contract]{en}* den hashCode erfüllen muss.
Andere Objekte verlassen sich darauf, dass dieser Vertrag erfüllt wird. Zum Beispiel die HäshMäp.

Schauen wir uns diesen Vertrag genauer an.

(pause: 1)

Der hashCode für ein Objekt muss wiederholbar das gleiche Ergebnis liefern.
Zwei Objekte die per equals-Vergleich übereinstimmen müssen den gleichen hashCode haben.
Falls der hashCode zweier Objekte gleich ist bedeutet das aber nicht, dass diese per equals-Vergleich als gleich gelten müssen.

Daher ist es wichtig, dass wenn man die equals Methode überschreibt, auch die hashCode Methode überschreibt und dafür sorgt, dass der hashCode Vertrag gültig ist.

```md
## hashCode Vertrag
- wiederholbar das gleiche Ergebnis
- wenn o1.equals(o2) == true,
    - muss auch o1.hashCode() ==  o2.hashCode() gelten
- falls o1.equals(o2) == false,
    - kann der Hash unterschiedlich sein, muss aber nicht
```

---

Als Beispiel schauen wir uns an wie sich verschiedene Objekte verhalten, wenn man sie als Key in einer HäshMäp verwendet und anschließend den verknüpften Wert abfragen möchte.

(pause: 1)

Wir werden Äpfel erstellen und deren Anzahl in einer HäshMäp hinterlegen.

(pause: 1)

```java
var greenApple = new Apple("green");
var anotherGreenApple = new Apple("green");
var redApple = new Apple("red");

HashMap<Apple, Integer> appleMap = new HashMap<>();
appleMap.put(greenApple, 10);
appleMap.put(redApple, 20);


appleMap.get(anotherGreenApple));
```

---
Für das erste Beispiel verwenden wir eine Implementierung bei der weder equals noch hashCode überschrieben wurden. Das führt dazu, dass die Implementierung von Object verwendet wird. Nochmal zu Erinnerung: in Java erbt jedes Objekt von Object. Darunter fällt nicht nur hashCode sondern auch zum Beispiel die equals, die to-String und die clone Methode.

(pause: 1)

```java
public class PlainApple {

    private String color;

    public PlainApple(String color) {
        this.color = color;
    }

    //equals and hashCode are used from parent (Object)
}
```
---

Lasst uns ein paar Äpfel erstellen.

(pause: 1)

Einen grünen, noch einen grünen und einen roten. Wie verhalten sich nun die Objekte?

(pause: 1)

```java
var greenApple = new PlainApple("green");
var anotherGreenApple = new PlainApple("green");
var redApple = new PlainApple("red");
```

---
Wie wir im Video über equals schon gelernt haben, werden die beiden grünen Äpfel nicht als gleich angesehen, da die equals Methode von Object auf Identität prüft.

(pause: 1)

Für die hashCodes unserer Äpfel sieht es so aus: Jedes Objekt hat einen anderen hashCode. Dies verletzt nicht den hashCode Vertrag, da die Objekte bei einem equals Vergleich nicht als gleich angesehen werden. Daher müssen die hashes auch nicht gleich sein.

(pause: 1)

```java
anotherGreenApple.hashCode(); // 918221580
greenApple.hashCode(); // 2055281021
redApple.hashCode(); // 1554547125
```
---

Falls wir die Implementierung ohne eigene equals und hashCode Methoden wie hier zu sehen zu einer HäshMäp hinzufügen und mit einem Objekt anfragen, welches die gleichen Eigenschaften hat, bekommen wir nall als Ergebnis zurück.
Das liegt daran, dass sich kein Objekt in der map befindet, welches den gleichen hash hat und equals zu dem angefragten ist.

(pause: 1)

```java
var greenApple = new PlainApple("green");
var anotherGreenApple = new PlainApple("green");
var redApple = new PlainApple("red");

//hashMap stores apple type and its quantity
HashMap<PlainApple, Integer> appleMap = new HashMap<>();
appleMap.put(greenApple, 10);
appleMap.put(redApple, 20);

appleMap.get(anotherGreenApple); // => null
```
---

Was passiert nun wenn wir einen Schritt weitergehen und equals implementieren?

(pause: 1)

Die hier gezeigte equals Methode wurde von der IDE generiert.
Diese gibt true zurück falls die Objekte ein und das selbe sind oder sie die gleiche Farbe haben.

(pause: 1)

(font-size: 40)
```java
public class OnlyEqualsApple {
    private String color;

    public OnlyEqualsApple(String color) {
        this.color = color;
    }

    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof OnlyEqualsApple))
            return false;
        if (obj == this)
            return true;
        return this.color.equals(((OnlyEqualsApple) obj).color);
    }
}
```

---
Wir erstellen wieder 3 Äpfel mit den gleichen Eigenschaften wie zuvor, 2 grüne und einen roten Apfel.

(pause: 1)

Durch das Hinzufügen der equals Methode gelten nun unsere beiden grünen Äpfel als gleich.

```java
var greenApple = new OnlyEqualsApple("green");
var anotherGreenApple = new OnlyEqualsApple("green");
var redApple = new OnlyEqualsApple("red");
```
---

Nach wie vor sind die hashes alle unterschiedlich. Das ist auch wenig verwunderlich, da wir an der Implementierung von hashCode nichts geändert haben. Daher wird weiterhin die Implementierung aus Object verwendet.

(pause: 1)

Allerdings verletzen wir nun den hashCode Vertrag, da die beiden grünen Äpfel per equals Vergleich als gleich gelten, sie aber unterschiedliche hashes haben.

```java
greenApple.hashCode(); // 1421795058
anotherGreenApple.hashCode(); // 804564176
redApple.hashCode(); // 1555009629
```
---
Wie verhält sich nun die HäshMäp bei get für einen grünen Apfel?

(pause: 1)

Das get liefert immer noch nall, da die beiden grünen Äpfel in unterschiedlichen Hashbackets landen. Daher kommt es gar nicht zu einem equals Vergleich. Aus diesem Grund kann man mit den zweiten grünen Apfel nicht den Wert, der mit dem ersten hinterlegt wurde auslesen.

(pause: 1)

```java
var greenApple = new OnlyEqualsApple("green");
var anotherGreenApple = new OnlyEqualsApple("green");
var redApple = new OnlyEqualsApple("red");

//hashMap stores apple type and its quantity
HashMap<OnlyEqualsApple, Integer> appleMap = new HashMap<>();
appleMap.put(greenApple, 10);
appleMap.put(redApple, 20);

appleMap.get(anotherGreenApple); // => null
```
---

Damit die HäshMäp unsere Objekte so verwaltet werden wie wir es erwarten müssen wir eine Implementierung zur Verfügung stellen, welche den hashCode Vertrag erfüllt.
Die gezeigte Implementierung zeigt equals und hashCode wie sie von der IDE generiert werden.

(pause: 1)

(font-size: 40)
```java
public class IntellijApple {

    private String color;

    public IntellijApple(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final IntellijApple that = (IntellijApple) o;

        return color != null ? color.equals(that.color) : that.color == null;
    }

    @Override
    public int hashCode() {
        return color != null ? color.hashCode() : 0;
    }
}

```

---
Wie sieht es denn nun bei den hashes aus? Sehr gut, die beiden grünen Äpfel haben den gleichen hash.

(pause: 1)

```java
greenApple.hashCode(); // 98619139
anotherGreenApple.hashCode(); // 98619139
redApple.hashCode(); // 112785
```
---

Nun funktioniert auch das Abfragen der Anzahl mit Hilfe des grünen Apfels wie gewünscht. Wir bekommen mit der Instanz unseres zweiten grünen Apfels den Wert aus der HäshMäp den wir zuvor mit dem ersten grünen Apfel hinterlegt haben.

(pause: 1)

```java
var greenApple = new IntellijApple("green");
var anotherGreenApple = new IntellijApple("green");
var redApple = new IntellijApple("red");

//hashMap stores apple type and its quantity
HashMap<IntellijApple, Integer> appleMap = new HashMap<>();
appleMap.put(greenApple, 10);
appleMap.put(redApple, 20);

appleMap.get(anotherGreenApple); // => 10
```

---

Um zu verdeutlichen, dass es hier nicht darauf ankommt, dass die hashes des roten und der grünen Äpfel unterschiedlich sind, bringen wir eine etwas fragwürdige Implementierung der hashCode Methode ins Spiel. Die equals-Methode ist unverändert, die hashCode Methode gibt immer 42 zurück.

(pause: 1)

Eine solche Implementierung ist sehr gefährlich und verwandelt unsere HäshMäp in eine Liste, da sich alle Einträge hinter einem hash verbergen. Damit kann sich auch die Laufzeit erheblich verschlechtern. Die Laufzeit ändert sich von linear zu quadratisch!

(pause: 1)



(font-size: 35)
```java
public class UselessHashCodeApple {

    private String color;

    public UselessHashCodeApple(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final UselessHashCodeApple that = (UselessHashCodeApple) o;

        return color != null ? color.equals(that.color) : that.color == null;
    }

    @Override
    public int hashCode() {
        return 42;
    }
}
```

---
Tatsächlich funktioniert auch diese Implementierung, obwohl alle Objekte den gleichen Hash haben.
Der hashCode Vertrag ist erfüllt und somit arbeitet auch die HäshMäp korrekt.
Allerdings hat sie durch die Art und Weise wie hashCode implementiert ist ihren Geschwindigkeitsvorteil beim anfragen gegenüber einer Liste verloren.

(pause: 1)

(font-size: 40)
```java
var greenApple = new UselessHashCodeApple("green");
var anotherGreenApple = new UselessHashCodeApple("green");
var redApple = new UselessHashCodeApple("red");

//hashMap stores apple type and its quantity
HashMap<UselessHashCodeApple, Integer> appleMap = new HashMap<>();
appleMap.put(greenApple, 10);
appleMap.put(redApple, 20);

appleMap.get(anotherGreenApple); // => 10
```

---

Dass der hashCode Vertrag verletzt wird, kann sehr einfach im Laufe der Zeit passieren.
Ein Objekt entwickelt sich weiter und bekommt neue Attribute.

(pause: 1)

Diese fließen wahrscheinlich in die hashCode und equals Methode ein. Dabei darf aber nicht der hashCode Vertrag verletzt werden. Es ist wichtig, dass alle Attribute die den equals check beeinflussen auch Teil der hashCode Methode sind.
Daher empfiehlt es sich die equals und hashCode Methoden nach Möglichkeit generieren zu lassen, entweder von der IDE oder von Tools wie Lombok.

```java
@RequiredArgsConstructor
@Data
@EqualsAndHashCode
public class LombokApple {
    private final String color;
}
```

---

Falls man immutable Objekte hat, ist es auch möglich den hashCode einmalig zu generieren und diesen zu speichern. Wenn Performance ein kritischer Faktor ist, kann dies helfen schneller zu werden. Vor allem wenn die Generierung des hashes aufwendig ist.
Die Objects Klasse bietet eine Methode an, die die hashCode Berechnung übernehmen kann und eine sehr lesbare Option ist. Die Methode kann beliebig viele Objekte entgegennehmen.

(pause: 2)

(font-size: 30)
```java
public class ImmutableApple {

    private final String color;
    private int hash;

    public ImmutableApple(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ImmutableApple that = (ImmutableApple) o;
        return Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {
        if (hash == 0) {
            hash = Objects.hash(color);
        }
        return hash;
    }
}
```

---

weitere Infos und den Code findet ihr hier.
Vielen Dank für eure Zeit und bis bald!

```md
## weitere Infos
- https://bitbucket.org/esentri/coding-essentials/src/master/source/code/
- Buchempfehlung: Effective Java - Joshua Bloch

```

---
