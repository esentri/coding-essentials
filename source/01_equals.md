---
size: 1080p
#canvas: "#32373c"
transition: crossfade 0.2
#background: music/background.mp3 0.05 fade-out fade-in
#theme: default
theme: custom.css
subtitles: embed
voice: Ilse

---
![contain](slides/01_equals/titel.png)

Hallo und herzlich Willkommen bei den "esenntrie coding essentials". Heute steht die equals Methode im Fokus.

(pause: 1)
---
In der Tschawadoc für equals in Object ist die Rede von einem *[general contract]{en}* den equals erfüllen muss. Schauen wir uns diesen Vertrag genauer an.

(pause: 1)

equals muss reflexiv, symmetrisch, transitiv und konstant sein. Außerdem muss ein Vergleich mit nall in false resultieren.

Schauen wir uns die einzelnen Punkte im Detail an.

(pause: 1)

```md
## equals Vertrag
- reflexiv
- symmetrisch
- transitiv
- konstant
- x.equals(null) -> false
```

---

Reflexiv bedeutet, dass das Objekt zu sich selbst equal sein muss.

(pause: 1)

~~~md
## reflexiv
ein Objekt muss zu sich selbst equals sein

    x.equals(x) == true

~~~

---

Symmetrisch bedeutet, dass wenn ein Objekt `X` zu einem Objekt `Y` equal ist dann muss auch Objekt `Y` zu Objekt `X` equal sein.

(pause: 1)

```md
## symmetrisch
wenn x.equals(y), dann muss auch y.equals(x) gelten

    x.equals(y) == true
    y.equals(x) == true
```

---

Transitiv bedeutet, dass wenn ein Objekt `X` zu einem Objekt `Y` equal und Objekt `Y` zu Objekt `Z` equal ist,
dann muss auch Objekt `X` zu Objekt `Z` equal sein.

(pause: 1)

```md
## transitiv
wenn x.equals(y) und y.equals(z)), dann muss auch x.equals(z)) gelten

    x.equals(y) == true
    y.equals(z) == true
    x.equals(z) == true
```

---

Konstant bedeutet, dass solange keine Werte in `X` und `Y` geändert werden der equals check immer das gleiche Ergebnis liefern muss.

(pause: 1)

```md
## konstant
wenn X und Y unverändert sind ist auch das Ergebnis von equals unverändert

    x.equals(y) == true
    // wait some time
    x.equals(y) == true
```

---

Eine Prüfung gegen einen nall Wert muss immer false liefern.

(pause: 1)

```md
## x.equals(null) -> false
wenn gegen null verglichen wird, muss false zurückgegeben werden

    x.equals(null) == false
```

---

Es ist einfach equals Methoden zu schreiben, welche den equals Vertrag verletzen.
Schauen wir uns diese Klasse an, welche einen case-insensitive String implementiert.

(pause: 1)


In der equals Methode wurde auch der Vergleich zu einem normalen String umgesetzt, damit Vergleiche einfacher sind.
Das Problem ist, dass diese Implementierung die geforderte Symmetrie verletzt.

(pause: 1)

(font-size: 35)
```java
public class CaseInsensitiveString {

    private final String s;

    public CaseInsensitiveString(final String s) {
        this.s = Objects.requireNonNull(s);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof CaseInsensitiveString) {
            return s.equalsIgnoreCase(((CaseInsensitiveString) obj).s);
        }
        if (obj instanceof String) {
            return s.equalsIgnoreCase((String) obj);
        }
        return false;
    }
}
```

---

Wie dieser Test beweist, ist die Symmetrie bei dieser equals Methode nicht gegeben. Der case-insensitive String ist equals zu dem normalen String, aber nicht umgekehrt.

(pause: 1)

Um die Symmetrie wiederherzustellen, muss man den Teil für den String-Vergleich entfernen.

(pause: 1)

(font-size: 40)
```java
@Test
void showSymmetryIsBroken() {

    var exampleString = "I have upper and lower case in ME!";
    var caseInsensitiveString = new CaseInsensitiveString(exampleString);
    assertThat(caseInsensitiveString).isEqualTo(exampleString); // => passes
    assertThat(exampleString).
            as("Symmetry is broken")
            .isEqualTo(caseInsensitiveString); // => fails
}
```

---

Auch bei Vererbung lässt sich die Symmetrie recht leicht verletzen. Gegeben ist die folgende Point Klasse:
Diese hat eine equals Methode die die Anforderung des equals Vertrags erfüllt.

(pause: 2)

(font-size: 40)
```java
public class Point {

    private final int x;
    private final int y;

    public Point(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;
        final Point point = (Point) o;
        return x == point.x && y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
```

---
Nun möchten wir diese Klasse erweitern und das Attribut "Farbe" hinzufügen. Es stellt sich die Frage wir man eine
korrekte equals Methode implementiert.
Falls man auf die Implementierung der super Methode aus "Point" zurückgreift,
sind alle Punkte gleich die an der gleichen Position sind, ungeachtet ihrer Farbe. Das ist nicht das gewünschte Verhalten.

(pause: 1)

Man könnte wie hier zu sehen in der equals Methode super punkt equals aufrufen und anschließend noch auf die Farbe prüfen.

Allerdings verletzt man mit diesem Ansatz die Symmetrie.

(pause: 1)

(font-size: 35)
```java
@Override
public boolean equals(final Object o) {
    if (!(o instanceof ColorPointViolatingSymmetry)) {
        return false;
    }
    return super.equals(o) && ((ColorPointViolatingSymmetry) o).color == color;
}
```

---

Wie dieser Test zeigt, resultiert ein Vergleich von einen Point mit einem ColorPoint in true.
Vergleicht man den ColorPoint mit dem Point resultiert der equals Vergleich in false.

(pause: 2)


(font-size: 35)
```java
@Test
 void showSymmentryIsBroken() {
     var colorPoint1 = new ColorPointViolatingSymmetry(Color.BLACK, 1, 1);
     var point = new Point(1, 1);

     assertThat(point)
             .isEqualTo(colorPoint1); // => passes
     assertThat(colorPoint1)
             .as("if not, symmetry is broken")
             .isEqualTo(point); // => fails
 }
```

---

Das Problem der Symmetrie lässt sich mit dieser Implementierung von equals lösen, bringt dafür aber ein anderes mit sich.
Die Idee bei dieser Implementierung ist, dass man sofern beide Objekte Instanzen von "Point" sind, sie sich vergleichen lassen.
Falls "o" kein ColorPoint ist, wird ein Vergleich unter Vernachlässigung der Farbe durchgeführt.

(pause: 2)

(font-size: 35)
```java
@Override
public boolean equals(final Object o) {
    if (!(o instanceof Point)) {
        return false;
    }
    if (!(o instanceof ColorPointViolatingTransitivity)) {
        return o.equals(this); //color blind equals
    }
    return super.equals(o) && ((ColorPointViolatingTransitivity) o).color == color;
}
```
---

Für diesen Test erstellen wir 3 Punkte, zwei mit Farbinformationen, einen ohne. Alle bekommen die gleichen Koordinaten.
colorPoint1 ist gleich zu point und point ist gleich zu colorPoint2. Laut dem equals Vertrag müsste nun auch colorPoint1
gleich zu colorPoint2 sein.

(pause: 1)

Wie der Test zeigt, ist das nicht so.

(pause: 1)


(font-size: 35)
```java
    @Test
    void showTransitivityIsBroken() {
        var colorPoint1 = new ColorPointViolatingTransitivity(Color.BLACK, 1, 1);
        var point = new Point(1, 1);
        var colorPoint2 = new ColorPointViolatingTransitivity(Color.RED, 1, 1);

        assertThat(colorPoint1)
                .isEqualTo(point);      // => passes

        assertThat(point)
                .isEqualTo(colorPoint2); // => passes

        assertThat(colorPoint1)
                .as("if not, transitivity is broken")
                .isEqualTo(colorPoint2); // => fails
    }
```

---

Nun könnte man auf die Idee kommen und den instance of check durch einen check mit get Class zu ersetzen.
Das ist genau das was intelli Tschäj macht, wenn man eine equals Methode generiert.
Diese Implementierung hat ebenfalls ein Problem. Sie verletzt das liskovsche Substitutionsprinzip.
Welches besagt, dass Kind- und Elternklassen austauschbar sein müssen, sodass der Code ungeachtet der tatsächlichen
Implementierung funktioniert.

(pause: 1)


(font-size: 30)
```java
public class IntellijPoint {

    private final int x;
    private final int y;

    public IntellijPoint(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final IntellijPoint that = (IntellijPoint) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
```

---
Nehmen wir an, dass wir eine Unterklasse erstellen möchten, die nur die Anzahl der instanziierten Objekte zählen möchte.
Es werden keine weiteren Felder hinzugefügt oder die equals Methode überschrieben.

(pause: 1)


(font-size: 40)
```java
public class CounterPoint extends IntellijPoint {

    private static final AtomicInteger counter = new AtomicInteger();

    public CounterPoint(final int x, final int y) {
        super(x, y);
        counter.incrementAndGet();
    }

    public static int numberCreated() {
        return counter.get();
    }
}
```
---
Nun wollen wir prüfen ob ein Punkt auf einem definierten Pfad liegt.
Dazu haben wir alle Punkte die diesen Pfad beschreiben zu einem Set hinzugefügt.

(pause: 1)

Das liskovsche Substitutionsprinzip fordert nun, dass ich die beiden Klassen gleich verhalten sollen, da die eine von der
andern erbt.

(pause: 1)


Leider ist das nicht so, obwohl die Kindklasse die gleichen Koordinaten hat, schlägt der contains check fehl.

(pause: 1)

(font-size: 40)
```java
@Test
void showThatLiskovIsViolated() {   
     var points = Set.of(
            new IntellijPoint(0, 0),
            new IntellijPoint(1, 0),
            new IntellijPoint(0, 1),
            new IntellijPoint(1, 1)
    );  

    IntellijPoint point = new IntellijPoint(1, 1);
    assertThat(points)
            .as("(1,1) should be in set")
            .contains(point); // => passes

    IntellijPoint actuallyACounterPoint = new CounterPoint(1, 1);  
    assertThat(points)
            .as("(1,1) should be in set")
            .contains(actuallyACounterPoint); // => fails
}
```

---

Wie lässt sich nun dieses Problem lösen? Hier ist es klug auf die Vererbung zu verzichten und stattdessen eine Komposition
zu wählen, welche eine View auf das gewünschte Objekt zur Verfügung stellt.

(pause: 1)

Mit diesem Ansatz umgeht man das Problem mit dem liskovschen Substitutionsprinzip, da es keine Kindklasse von Point gibt.

(pause: 1)

(font-size: 35)
```java
public class ColorPointComposition {

    private final Point point;
    private final Color color;

    public ColorPointComposition(final Color color, final int x, final int y) {
        this.point = new Point(x, y);
        this.color = color;
    }

    public Point asPoint() {
        return point;
    }

    //equals (...)
    //hashCode (...)
}

```

---

weitere Infos und den Code findet ihr hier. Zum Beispiel eine Anleitung wie man eine equals Methode schreibt.

(pause: 1)

Vielen Dank, und bis zum nächsten Mal!

```md
## weitere Infos
- https://www.artima.com/articles/how-to-write-an-equality-method-in-java
- https://bitbucket.org/esentri/coding-essentials/src/master/source/code/
- Buchempfehlung: Effective Java - Joshua Bloch

```

---
