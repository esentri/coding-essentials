package de.esentri.coding.essentials.rudiments._03_immutable;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class ImmutablePointTest {

    private static Stream<Arguments> providePoints() {

        List<Point> neighbors = List.of(
                new MutablePoint(1, 2, List.of(), new StringBuilder()),
                new MutablePoint(1, 3, List.of(), new StringBuilder())
        );

        return Stream.of(
                Arguments.of(ImmutablePoint.of(1, 1, neighbors, new StringBuilder(ImmutablePoint.class.getSimpleName()))),
                Arguments.of(new PointAsRecord(1, 1, neighbors, new StringBuilder(PointAsRecord.class.getSimpleName()))),
                Arguments.of(new ImmutableLombokPoint(1, 1, neighbors, new StringBuilder(ImmutableLombokPoint.class.getSimpleName())))
        );
    }

    @ParameterizedTest
    @MethodSource("providePoints")
    void containedList_SHOULD_NOT_beMutable(Point point) {

        assertThatThrownBy(() -> {
            point.getNeighbors().clear();
        }).isInstanceOf(UnsupportedOperationException.class);


    }

    @ParameterizedTest
    @MethodSource("providePoints")
    void stringBuilder_SHOULD_NOT_modifiableFromOutside(Point point) {

        var builder = point.getCommentBuilder();

        builder.append("\ntry to modify");

        assertThat(point.getCommentBuilder().toString())
                .isEqualTo(point.getClass().getSimpleName());

    }

}