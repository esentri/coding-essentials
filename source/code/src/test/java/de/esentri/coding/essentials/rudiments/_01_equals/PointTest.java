package de.esentri.coding.essentials.rudiments._01_equals;

import org.junit.jupiter.api.Test;

import java.awt.*;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class PointTest {

    @Test
    void showSuperEqualsDoesNotWork() {
        var colorPoint1 = new ColorPointSuperEqualsMethod(Color.BLACK, 1, 1);
        var colorPoint2 = new ColorPointSuperEqualsMethod(Color.WHITE, 1, 1);

        assertThat(colorPoint1)
                .as("point should not be equal")
                .isNotEqualTo(colorPoint2);
    }

    @Test
    void showSymmentryIsBroken() {
        var colorPoint1 = new ColorPointViolatingSymmetry(Color.BLACK, 1, 1);
        var point = new Point(1, 1);

        assertThat(point)
                .isEqualTo(colorPoint1);

        assertThat(colorPoint1)
                .as("symmetry broken -> should not fail because test before succeeded")
                .isEqualTo(point);
    }

    @Test
    void showTransitivityIsBroken() {
        var colorPoint1 = new ColorPointViolatingTrasitivity(Color.BLACK, 1, 1);
        var point = new Point(1, 1);
        var colorPoint2 = new ColorPointViolatingTrasitivity(Color.RED, 1, 1);

        assertThat(colorPoint1)
                .isEqualTo(point);

        assertThat(point)
                .isEqualTo(colorPoint2);

        assertThat(colorPoint1)
                .as("transitivity broken -> should not fail because tests before succeeded")
                .isEqualTo(colorPoint2);
    }

    @Test
    void showThatLiskovIsViolated() {

        var points = Set.of(
                new IntellijPoint(0, 0),
                new IntellijPoint(1, 0),
                new IntellijPoint(0, 1),
                new IntellijPoint(1, 1)
        );

        IntellijPoint point = new IntellijPoint(1, 1);
        assertThat(points)
                .as("(1,1) should be in set")
                .contains(point);

        IntellijPoint actuallyACounterPoint = new CounterPoint(1, 1);
        assertThat(points)
                .as("(1,1) should be in set")
                .contains(actuallyACounterPoint);
    }

}