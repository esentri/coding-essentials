package de.esentri.coding.essentials.libs.assertj.lordOfTheRings;

import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.LordOfTheRingSetting.elvesRings;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.LordOfTheRingSetting.fellowshipOfTheRing;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.LordOfTheRingSetting.frodo;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.LordOfTheRingSetting.galadriel;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.LordOfTheRingSetting.ringBearers;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.LordOfTheRingSetting.sam;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.Race.ELF;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.Race.HOBBIT;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.Race.ORC;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.Ring.DWARF_RING;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.Ring.MAN_RING;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.Ring.NARYA;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.Ring.NENYA;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.Ring.ONE_RING;
import static de.esentri.coding.essentials.libs.assertj.lordOfTheRings.Ring.VILYA;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;
import static org.assertj.core.api.Assertions.tuple;
import static org.assertj.core.api.Assertions.withMarginOf;
import static org.assertj.core.util.Lists.list;

import java.time.Duration;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


class LordOfTheRingSettingTest {

    private static final Logger logger = LoggerFactory.getLogger(LordOfTheRingSettingTest.class);
    private static final String ERROR_MESSAGE_EXAMPLE_FOR_ASSERTION = "{} assertion :\n{}\n-------------------------\n";

    @Test
    void test_iterables() {

        assertThat(elvesRings).hasSize(3);

        assertThat(elvesRings)
                .doesNotContainNull()
                .doesNotHaveDuplicates();

        assertThat(elvesRings)
                .contains(NENYA, NARYA)
                .doesNotContain(ONE_RING);

        assertThat(elvesRings).containsExactly(VILYA, NENYA, NARYA);

        try {
            assertThat(elvesRings).containsExactly(NARYA, VILYA, NENYA);
        } catch (AssertionError e) {
            logAssertionErrorMessage("containsExactly", e);
        }

        assertThat(elvesRings).containsAnyOf(NENYA, ONE_RING, DWARF_RING);

        try {
            assertThat(elvesRings).containsOnly(NENYA, VILYA, ONE_RING);
        } catch (AssertionError e) {
            logAssertionErrorMessage("containsOnly", e);
        }

        Iterable<Ring> allRings = list(ONE_RING, VILYA, NENYA, NARYA, DWARF_RING, MAN_RING);
        assertThat(allRings)
                .startsWith(ONE_RING, VILYA)
                .endsWith(DWARF_RING, MAN_RING);

        assertThat(allRings).containsOnlyOnce(ONE_RING);
    }

    @Test
    void test_extracting() {

        assertThat(fellowshipOfTheRing)
                //.extracting("race")
                .extracting(TolkienCharacter::getRace)
                .contains(HOBBIT, ELF)
                .doesNotContain(ORC);

        TolkienCharacter unknown = new TolkienCharacter("unknown", 100, null);
        assertThat(list(sam, unknown))
                .extracting("name", "age", "race.name")
                .contains(
                        tuple("Sam", 38, "Hobbit"),
                        tuple("unknown", 100, null)
                );
    }

    @Test
    void test_data_java8plus() {
        ZonedDateTime firstOfJanuary2000InUTC = ZonedDateTime.parse("2000-01-01T00:00:00Z");


        assertThat(firstOfJanuary2000InUTC).isEqualTo("2000-01-01T00:00:00Z");

        try {
            // 2000-01-01T00:00+01:00 = 1999-12-31T23:00:00Z !
            assertThat(firstOfJanuary2000InUTC).isEqualTo("2000-01-01T00:00+01:00");
        } catch (AssertionError e) {
            logAssertionErrorMessage("isEqualTo with time zone change adjustment", e);
        }

        ZonedDateTime zonedDateTime1 = ZonedDateTime.of(2000, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC);
        ZonedDateTime zonedDateTime2 = ZonedDateTime.of(2000, 1, 1, 23, 59, 59, 999, ZoneOffset.UTC);
        assertThat(zonedDateTime1).isEqualToIgnoringHours(zonedDateTime2);

        assertThat(zonedDateTime1).isBefore(zonedDateTime2);
        assertThat(zonedDateTime2).isAfter(zonedDateTime1);


        ZonedDateTime zonedDateTime = ZonedDateTime.now();
        assertThat(zonedDateTime)
                .isBetween(zonedDateTime.minusSeconds(1), zonedDateTime.plusSeconds(1))
                .isBetween(zonedDateTime, zonedDateTime.plusSeconds(1))
                .isStrictlyBetween(zonedDateTime.minusSeconds(1), zonedDateTime.plusSeconds(1)); //start and end excluded
    }

    @Test
    void test_duration() {

        assertThat(Duration.ofMinutes(15))
                .isCloseTo(Duration.ofMinutes(10), withMarginOf(Duration.ofMinutes(5)));

        try {
            assertThat(Duration.ofMinutes(15))
                    .isCloseTo(Duration.ofMinutes(10), withMarginOf(Duration.ofMinutes(3)));
        } catch (AssertionError e) {
            logAssertionErrorMessage("date isCloseTo", e);
        }
    }

    @Test
    void test_map() {
        assertThat(ringBearers)
                .isNotEmpty()
                .hasSize(4);

        assertThat(ringBearers)
                .contains(
                        entry(ONE_RING, frodo),
                        entry(NENYA, galadriel)
                );

        try {
            assertThat(ringBearers).containsOnlyKeys(NENYA, NARYA, DWARF_RING);
        } catch (AssertionError e) {
            logAssertionErrorMessage("containsOnlyKeys with not found and not expected keys.", e);
        }

        assertThat(ringBearers).size()
                               .isGreaterThan(1)
                               .isLessThanOrEqualTo(4)
                               .returnToMap()
                               .containsKeys(ONE_RING, NENYA, NARYA)
                               .containsEntry(ONE_RING, frodo);
    }

    protected static void logAssertionErrorMessage(String assertionContext, AssertionError e) {
        logger.info(ERROR_MESSAGE_EXAMPLE_FOR_ASSERTION, assertionContext, e.getMessage());
    }
}