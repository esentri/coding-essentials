package de.esentri.coding.essentials.rudiments._01_equals;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class CaseInsensitiveStringTest {


    @Test
    void showSymmetryIsBroken() {

        String exampleString = "I have upper and lower case in ME!";
        CaseInsensitiveString caseInsensitiveString = new CaseInsensitiveString(exampleString);

        assertThat(caseInsensitiveString).isEqualTo(exampleString); // => passes
        assertThat(exampleString).
                as("Symmetry is broken")
                .isEqualTo(caseInsensitiveString); // => fails
    }

}