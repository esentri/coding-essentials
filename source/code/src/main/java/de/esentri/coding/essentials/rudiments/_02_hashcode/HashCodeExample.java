package de.esentri.coding.essentials.rudiments._02_hashcode;

import java.util.HashMap;
import java.util.Objects;

public class HashCodeExample {

    public static void main(String[] args) {
        runExampleForPlainApple();
        runExampleForOnlyEqualsOverwritten();
        runExampleForLombokApple();
        runExampleForUselessHashCodeApple();
        runExampleForIntellijApple();
    }

    private static void runExampleForOnlyEqualsOverwritten() {
        System.out.println("----------OnlyEqualsOverwrittenApple--------------");

        var greenApple = new OnlyEqualsApple("green");
        var anotherGreenApple = new OnlyEqualsApple("green");
        var redApple = new OnlyEqualsApple("red");

        Objects.equals(greenApple, greenApple); // => true (greenApple == greenApple)
        Objects.equals(greenApple, anotherGreenApple); // => true
        Objects.equals(greenApple, redApple); // => false
        Objects.equals(anotherGreenApple, redApple); // => false

        System.out.println("hash greenApple apple: " + greenApple.hashCode());
        System.out.println("hash red apple: " + redApple.hashCode());
        System.out.println("hash anotherGreenApple: " + anotherGreenApple.hashCode());

        anotherGreenApple.hashCode(); // 804564176
        greenApple.hashCode(); // 1421795058
        redApple.hashCode(); // 1555009629


        //hashMap stores apple type and its quantity
        HashMap<OnlyEqualsApple, Integer> m = new HashMap<>();
        m.put(greenApple, 10);
        m.put(redApple, 20);
        System.out.println("result of get green apple: " + m.get(anotherGreenApple));
        System.out.println("green apples are equal: " + greenApple.equals(anotherGreenApple));
        System.out.println(" ===> This is BAD <===");
    }

    private static void runExampleForLombokApple() {
        System.out.println("----------LombokApple--------------");
        var greenApple = new LombokApple("greenApple");
        System.out.println("hash greenApple apple: " + greenApple.hashCode());

        var redApple = new LombokApple("red");
        System.out.println("hash red apple: " + redApple.hashCode());

        //hashMap stores apple type and its quantity
        HashMap<LombokApple, Integer> m = new HashMap<>();
        m.put(greenApple, 10);
        m.put(redApple, 20);
        var anotherGreenApple = new LombokApple("greenApple");
        System.out.println("hash anotherGreenApple: " + anotherGreenApple.hashCode());
        System.out.println("result of get green apple: " + m.get(anotherGreenApple));
        System.out.println("green apples are equal: " + greenApple.equals(anotherGreenApple));
    }

    private static void runExampleForPlainApple() {
        System.out.println("----------PlainApple--------------");
        var greenApple = new PlainApple("green");
        var anotherGreenApple = new PlainApple("green");
        var redApple = new PlainApple("red");

        Objects.equals(greenApple, greenApple); // => true (greenApple == greenApple)
        Objects.equals(greenApple, anotherGreenApple); // => false
        Objects.equals(greenApple, redApple); // => false
        Objects.equals(anotherGreenApple, redApple); // => false

        System.out.println("hash greenApple apple: " + greenApple.hashCode());
        System.out.println("hash red apple: " + redApple.hashCode());
        System.out.println("hash anotherGreenApple: " + anotherGreenApple.hashCode());

        anotherGreenApple.hashCode(); // 918221580
        greenApple.hashCode(); // 2055281021
        redApple.hashCode(); // 1554547125


        //hashMap stores apple type and its quantity
        HashMap<PlainApple, Integer> appleMap = new HashMap<>();
        appleMap.put(greenApple, 10);
        appleMap.put(redApple, 20);

        appleMap.get(anotherGreenApple); // => null

        System.out.println("result of get green apple: " + appleMap.get(anotherGreenApple));
        System.out.println("green apples are equal: " + greenApple.equals(anotherGreenApple));
    }

    private static void runExampleForIntellijApple() {
        System.out.println("----------IntellijApple()--------------");
        var greenApple = new IntellijApple("green");
        var anotherGreenApple = new IntellijApple("green");
        var redApple = new IntellijApple("red");

        Objects.equals(greenApple, greenApple); // => true (greenApple == greenApple)
        Objects.equals(greenApple, anotherGreenApple); // => false
        Objects.equals(greenApple, redApple); // => false
        Objects.equals(anotherGreenApple, redApple); // => false

        System.out.println("hash greenApple apple: " + greenApple.hashCode());
        System.out.println("hash red apple: " + redApple.hashCode());
        System.out.println("hash anotherGreenApple: " + anotherGreenApple.hashCode());

        anotherGreenApple.hashCode(); // 918221580
        greenApple.hashCode(); // 2055281021
        redApple.hashCode(); // 1554547125


        //hashMap stores apple type and its quantity
        HashMap<IntellijApple, Integer> appleMap = new HashMap<>();
        appleMap.put(greenApple, 10);
        appleMap.put(redApple, 20);

        appleMap.get(anotherGreenApple); // => null

        System.out.println("result of get green apple: " + appleMap.get(anotherGreenApple));
        System.out.println("green apples are equal: " + greenApple.equals(anotherGreenApple));
    }

    private static void runExampleForUselessHashCodeApple() {
        System.out.println("----------UselessHashCodeApple--------------");
        var greenApple = new UselessHashCodeApple("greenApple");
        var anotherGreenApple = new UselessHashCodeApple("greenApple");
        var redApple = new UselessHashCodeApple("red");

        System.out.println("hash greenApple apple: " + greenApple.hashCode());
        System.out.println("hash red apple: " + redApple.hashCode());
        System.out.println("hash anotherGreenApple: " + anotherGreenApple.hashCode());

        //hashMap stores apple type and its quantity
        HashMap<UselessHashCodeApple, Integer> m = new HashMap<>();
        m.put(greenApple, 10);
        m.put(redApple, 20);
        System.out.println("result of get green apple: " + m.get(anotherGreenApple));
        System.out.println("green apples are equal: " + greenApple.equals(anotherGreenApple));
    }
}
