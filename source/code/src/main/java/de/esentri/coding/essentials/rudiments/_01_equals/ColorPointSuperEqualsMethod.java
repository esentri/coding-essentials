package de.esentri.coding.essentials.rudiments._01_equals;

import java.awt.*;

public class ColorPointSuperEqualsMethod extends Point {
    private final Color color;

    public ColorPointSuperEqualsMethod(final Color color, final int x, final int y) {
        super(x, y);
        this.color = color;
    }
}
