package de.esentri.coding.essentials.rudiments._03_immutable;

import lombok.Value;

import java.util.ArrayList;
import java.util.List;

@Value
public class ImmutableLombokPoint implements Point {

    private final int x;
    private final int y;
    private final List<Point> neighbors;
    private final StringBuilder commentBuilder;

    public ImmutableLombokPoint(final int x,
                                final int y,
                                final List<Point> neighbors,
                                final StringBuilder commentBuilder) {
        this.x = x;
        this.y = y;
        this.neighbors = new ArrayList<>(neighbors);
        this.commentBuilder = new StringBuilder(commentBuilder);
    }
}
