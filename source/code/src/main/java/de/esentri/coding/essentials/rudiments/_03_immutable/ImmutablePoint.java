package de.esentri.coding.essentials.rudiments._03_immutable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ImmutablePoint implements Point {

    private final int x;
    private final int y;
    private final List<Point> neighbors;
    private final StringBuilder commentBuilder;

    private ImmutablePoint(final int x,
                           final int y,
                           final List<Point> neighbors,
                           final StringBuilder commentBuilder) {
        this.x = x;
        this.y = y;
        this.neighbors = new ArrayList<>(neighbors);
        this.commentBuilder = new StringBuilder(commentBuilder);
    }

    public static ImmutablePoint of(final int x, final int y, final List<Point> neighbors, final StringBuilder commentBuilder) {
        return new ImmutablePoint(x, y, neighbors, commentBuilder);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public List<Point> getNeighbors() {
        return Collections.unmodifiableList(neighbors);
    }

    public StringBuilder getCommentBuilder() {
        return new StringBuilder(commentBuilder);
    }

    @Override
    public String toString() {
        return "ImmutablePoint{" +
                "x=" + x +
                ", y=" + y +
                ", neighbors=" + neighbors +
                ", commentBuilder=" + commentBuilder +
                '}';
    }
}
