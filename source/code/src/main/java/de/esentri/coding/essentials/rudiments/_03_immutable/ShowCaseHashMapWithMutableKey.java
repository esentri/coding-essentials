package de.esentri.coding.essentials.rudiments._03_immutable;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShowCaseHashMapWithMutableKey {


    public static void main(String[] args) {
        Map<MutablePoint, String> pointsToRemark = new HashMap<>();

        var point1 = new MutablePoint(1, 1, List.of(), new StringBuilder());
        var point2 = new MutablePoint(2, 2, List.of(), new StringBuilder());

        pointsToRemark.put(point1, "Remark1");
        pointsToRemark.put(point2, "Remark2");

        System.out.println(pointsToRemark.size()); // => 2
        System.out.println(pointsToRemark.get(point1)); // => Remark1
        System.out.println(pointsToRemark.get(point2)); // => Remark2

        point1.setX(11); //change key

        System.out.println(pointsToRemark.size()); // => 2
        System.out.println(pointsToRemark.get(point1));// => null


    }

}
