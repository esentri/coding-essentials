package de.esentri.coding.essentials.rudiments._01_equals;

import java.util.concurrent.atomic.AtomicInteger;

public class CounterPoint extends IntellijPoint {

    private static final AtomicInteger counter = new AtomicInteger();

    public CounterPoint(final int x, final int y) {
        super(x, y);
        counter.incrementAndGet();
    }

    public static int numberCreated() {
        return counter.get();
    }
}
