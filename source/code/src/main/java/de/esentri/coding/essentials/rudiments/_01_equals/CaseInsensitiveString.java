package de.esentri.coding.essentials.rudiments._01_equals;

import java.util.Objects;

public class CaseInsensitiveString {

    private final String s;

    public CaseInsensitiveString(final String s) {
        this.s = Objects.requireNonNull(s);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof CaseInsensitiveString) {
            return s.equalsIgnoreCase(((CaseInsensitiveString) obj).s);
        }
        if (obj instanceof String) { //trying to be smart to check also against Strings -> see Test
            return s.equalsIgnoreCase((String) obj);
        }
        return false;
    }
}
