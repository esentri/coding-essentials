package de.esentri.coding.essentials.rudiments._01_equals;

import java.awt.*;

public class ColorPointComposition {

    private final Point point;
    private final Color color;

    public ColorPointComposition(final Color color, final int x, final int y) {
        this.point = new Point(x, y);
        this.color = color;
    }

    public Point asPoint() {
        return point;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof ColorPointComposition)) return false;

        final ColorPointComposition that = (ColorPointComposition) o;

        if (point != null ? !point.equals(that.point) : that.point != null) return false;
        return color != null ? color.equals(that.color) : that.color == null;
    }

    @Override
    public int hashCode() {
        int result = point != null ? point.hashCode() : 0;
        result = 31 * result + (color != null ? color.hashCode() : 0);
        return result;
    }
}
