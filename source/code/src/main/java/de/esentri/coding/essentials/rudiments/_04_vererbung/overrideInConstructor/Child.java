package de.esentri.coding.essentials.rudiments._04_vererbung.overrideInConstructor;

import java.time.Instant;

public final class Child extends Parent {

    private final Instant instant;

    Child() {
        instant = Instant.now();
    }

    @Override
    public void overrideMe() {
        System.out.println(instant);
    }
}