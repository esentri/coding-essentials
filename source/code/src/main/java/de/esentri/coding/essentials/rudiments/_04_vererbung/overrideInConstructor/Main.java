package de.esentri.coding.essentials.rudiments._04_vererbung.overrideInConstructor;

public class Main {
    public static void main(String args[]) {
        Child child = new Child();
        child.overrideMe();
    }
}
