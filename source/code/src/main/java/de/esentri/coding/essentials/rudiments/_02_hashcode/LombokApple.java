package de.esentri.coding.essentials.rudiments._02_hashcode;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
@EqualsAndHashCode
public class LombokApple {
    private final String color;
}
