package de.esentri.coding.essentials.rudiments._04_vererbung.composition;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

public class CustomSet<E> extends AbstractSet<E> {


    private HashSet<E> delegate = new HashSet();
    private int count = 0;


    @Override
    public Iterator<E> iterator() {
        return delegate.iterator();
    }

    @Override
    public int size() {
        return delegate.size();
    }

    @Override
    public boolean add(E element) {
        count++;
        return super.add(element);
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        count += collection.size();
        return super.addAll(collection);
    }

    public int getCount() {
        return count;
    }
}
