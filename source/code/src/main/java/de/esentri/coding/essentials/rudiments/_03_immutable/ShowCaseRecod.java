package de.esentri.coding.essentials.rudiments._03_immutable;

import java.util.List;

public class ShowCaseRecod {

    public static void main(String[] args) {
        final StringBuilder builder = new StringBuilder("build");
        var point = new PointAsRecord(1, 1, List.of(), builder);

        builder.append("eine Änderung von außen");

        String result = builder.toString();

        System.out.println(point.commentBuilder());

    }
}
