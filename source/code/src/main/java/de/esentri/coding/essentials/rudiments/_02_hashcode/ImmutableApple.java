package de.esentri.coding.essentials.rudiments._02_hashcode;

import java.util.Objects;

public class ImmutableApple {

    private final String color;
    private int hash;

    public ImmutableApple(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final ImmutableApple that = (ImmutableApple) o;
        return Objects.equals(color, that.color);
    }

    @Override
    public int hashCode() {
        if (hash == 0) {
            hash = Objects.hash(color);
        }
        return hash;
    }
}
