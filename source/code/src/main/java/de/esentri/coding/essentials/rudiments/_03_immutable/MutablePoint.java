package de.esentri.coding.essentials.rudiments._03_immutable;

import java.util.List;

public class MutablePoint implements Point {

    private int x;
    private int y;
    private List<Point> neighbors;
    private StringBuilder commentBuilder;

    public MutablePoint(int x, int y, List<Point> neighbors, StringBuilder commentBuilder) {
        this.x = x;
        this.y = y;
        this.neighbors = neighbors;
        this.commentBuilder = commentBuilder;
    }

    public int getX() {
        return x;
    }

    public void setX(final int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(final int y) {
        this.y = y;
    }

    @Override
    public List<Point> getNeighbors() {
        return neighbors;
    }

    private void setNeighbors(final List<Point> neighbors) {
        this.neighbors = neighbors;
    }

    @Override
    public StringBuilder getCommentBuilder() {
        return commentBuilder;
    }

    private void setCommentBuilder(final StringBuilder commentBuilder) {
        this.commentBuilder = commentBuilder;
    }

    @Override
    public String toString() {
        return "MutablePoint{" +
                "x=" + x +
                ", y=" + y +
                ", neighbors=" + neighbors +
                ", commentBuilder=" + commentBuilder +
                '}';
    }
}
