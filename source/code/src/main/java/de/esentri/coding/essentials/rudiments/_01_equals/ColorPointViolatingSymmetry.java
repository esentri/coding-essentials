package de.esentri.coding.essentials.rudiments._01_equals;

import java.awt.*;

public class ColorPointViolatingSymmetry extends Point {
    private final Color color;

    public ColorPointViolatingSymmetry(final Color color, final int x, final int y) {
        super(x, y);
        this.color = color;
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof ColorPointViolatingSymmetry)) {
            return false;
        }
        return super.equals(o) && ((ColorPointViolatingSymmetry) o).color == color;
    }
}
