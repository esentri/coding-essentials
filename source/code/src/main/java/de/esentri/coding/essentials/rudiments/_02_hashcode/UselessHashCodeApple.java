package de.esentri.coding.essentials.rudiments._02_hashcode;

public class UselessHashCodeApple {

    private String color;

    public UselessHashCodeApple(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final UselessHashCodeApple that = (UselessHashCodeApple) o;

        return color != null ? color.equals(that.color) : that.color == null;
    }

    @Override
    public int hashCode() {
        return 42;
    }
}
