package de.esentri.coding.essentials.rudiments._01_equals;

import java.util.Objects;

public class IntellijPoint {

    private final int x;
    private final int y;


    public IntellijPoint(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final IntellijPoint that = (IntellijPoint) o;
        return x == that.x && y == that.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }
}
