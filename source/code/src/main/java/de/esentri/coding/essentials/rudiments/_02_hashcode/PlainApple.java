package de.esentri.coding.essentials.rudiments._02_hashcode;

public class PlainApple {

    private String color;

    public PlainApple(String color) {
        this.color = color;
    }

    //equals and hashCode are used from parent (Object)
}
