package de.esentri.coding.essentials.rudiments._01_equals;

import java.awt.*;

public class ColorPointViolatingTrasitivity extends Point {
    private final Color color;

    public ColorPointViolatingTrasitivity(final Color color, final int x, final int y) {
        super(x, y);
        this.color = color;
    }

    @Override
    public boolean equals(final Object o) {
        if (!(o instanceof Point)) {
            return false;
        }
        if (!(o instanceof ColorPointViolatingTrasitivity)) {
            return o.equals(this); //color blind equals
        }
        return super.equals(o) && ((ColorPointViolatingTrasitivity) o).color == color;
    }
}
