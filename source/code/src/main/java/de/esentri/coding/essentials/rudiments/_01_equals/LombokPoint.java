package de.esentri.coding.essentials.rudiments._01_equals;

public class LombokPoint {

    private final int x;
    private final int y;

    public LombokPoint(final int x, final int y) {
        this.x = x;
        this.y = y;
    }

    public boolean equals(final Object o) {
        if (o == this) return true;
        if (!(o instanceof LombokPoint)) return false;
        final LombokPoint other = (LombokPoint) o;
        if (!other.canEqual((Object) this)) return false;
        if (this.x != other.x) return false;
        if (this.y != other.y) return false;
        return true;
    }

    protected boolean canEqual(final Object other) {
        return other instanceof LombokPoint;
    }

    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        result = result * PRIME + this.x;
        result = result * PRIME + this.y;
        return result;
    }
}
