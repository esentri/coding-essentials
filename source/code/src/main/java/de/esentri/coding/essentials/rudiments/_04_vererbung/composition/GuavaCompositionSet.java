package de.esentri.coding.essentials.rudiments._04_vererbung.composition;

import com.google.common.collect.ForwardingSet;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class GuavaCompositionSet<E> extends ForwardingSet<E> {

    private HashSet<E> delegate = new HashSet();
    private int addCount = 0;

    @Override
    protected Set<E> delegate() {
        return delegate;
    }

    @Override
    public boolean add(E element) {
        addCount++;
        return super.add(element);
    }

    @Override
    public boolean addAll(Collection<? extends E> collection) {
        addCount += collection.size();
        return super.addAll(collection);
    }

    public int getAddCount() {
        return addCount;
    }
}
