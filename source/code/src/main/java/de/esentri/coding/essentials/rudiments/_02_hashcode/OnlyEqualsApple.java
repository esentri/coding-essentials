package de.esentri.coding.essentials.rudiments._02_hashcode;

public class OnlyEqualsApple {
    private String color;

    public OnlyEqualsApple(String color) {
        this.color = color;
    }

    public boolean equals(Object obj) {
        if (obj == null) return false;
        if (!(obj instanceof OnlyEqualsApple))
            return false;
        if (obj == this)
            return true;
        return this.color.equals(((OnlyEqualsApple) obj).color);
    }
}