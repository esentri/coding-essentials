package de.esentri.coding.essentials.rudiments._02_hashcode;

public class IntellijApple {

    private String color;

    public IntellijApple(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        final IntellijApple that = (IntellijApple) o;

        return color != null ? color.equals(that.color) : that.color == null;
    }

    @Override
    public int hashCode() {
        return color != null ? color.hashCode() : 0;
    }
}
