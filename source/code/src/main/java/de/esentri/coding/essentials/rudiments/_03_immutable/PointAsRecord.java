package de.esentri.coding.essentials.rudiments._03_immutable;

import java.util.List;

public record PointAsRecord(int x, int y, List<Point> neighbors,
                            StringBuilder commentBuilder) implements Point {

    public PointAsRecord {
        commentBuilder = new StringBuilder(commentBuilder);
    }

    @Override
    public int getX() {
        return x();
    }

    @Override
    public int getY() {
        return y();
    }

    @Override
    public List<Point> getNeighbors() {
        return neighbors();
    }

    @Override
    public StringBuilder getCommentBuilder() {
        return commentBuilder();
    }
}
