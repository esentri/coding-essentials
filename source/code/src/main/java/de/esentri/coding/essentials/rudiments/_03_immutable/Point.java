package de.esentri.coding.essentials.rudiments._03_immutable;

import java.util.List;

public interface Point {

    int getX();

    int getY();

    List<Point> getNeighbors();

    StringBuilder getCommentBuilder();
}
