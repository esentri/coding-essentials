package de.esentri.coding.essentials.rudiments._04_vererbung.overrideInConstructor;

public class Parent {
    //Broken
    public Parent() {
        overrideMe();
    }

    protected void overrideMe() {
    }
}