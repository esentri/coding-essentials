package de.esentri.coding.essentials.rudiments._03_immutable;

public class StringShowCase {

    public static void main(String[] args) {
        String string1 = "Be yourself is all that you can do";
        String string2 = string1.toLowerCase();
        System.out.println(string1); // => Be yourself is all that you can do
        System.out.println(string2); // => be yourself is all that you can do
    }


}
