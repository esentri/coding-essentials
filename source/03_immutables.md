---
size: 1080p
#canvas: "#32373c"
transition: crossfade 0.2
#background: music/background.mp3 0.05 fade-out fade-in
#theme: default
theme: custom.css
subtitles: embed
voice: Ilse

---

Hallo und herzlich Willkommen bei den "esenntrie coding essentials". Heute stehen immutable Klassen im Fokus.

(pause: 1)


![contain](slides/03_immutables/titel.png)
---
Was sind immutable Objekte? Warum können sie einem das Leben erleichtern und wie erstellt man sie?
Für all diese Fragen gibt es Antworten in diesem Video!

```md
## Wie? Wo? Was? Warum?
```

---

Was sind nun immutable Objekte. immutable oder unveränderliche Objekte sind Objekte die ihren Zustand nicht mehr verändern sobald sie
erstellt wurden. Ein sehr prominentes Beispiel ist das String Objekt.

(pause: 1)

```md
## Was?
- unveränderlich
- => haben immer den gleichen Zustand
```

---

Alle Methoden die Veränderungen an einem String bedeuten, geben einen neuen String zurück, auf den die Änderung angewendet wurde. Der ursprüngliche String bleibt unverändert.
Wie in diesem Beispiel zu sehen ist, bleibt string1 nach dem to lower case Aufruf unverändert. In string2 ist die veränderte Version von string1 hinterlegt.

(pause: 1)

(font-size: 40)
```java
String string1 = "Be yourself is all that you can do";
String string2 = string1.toLowerCase();
System.out.println(string1); // => be yourself is all that you can do
System.out.println(string2); // => be yourself is all that you can do
```

---

Warum ist es nun von Vorteil unveränderliche Objekte zu nutzen?
Immutable Objekte sind immer in einem validen Zustand sobald sie erstellt wurden. Da sie nicht verändert werden können, bleiben sie das auch für immer. Falls beim Aufruf einer Methode eine Exception geworfen wird, bleibt das Objekt auf dem die Methode aufgerufen wurde unverändert und damit valide. Das kann für retries oder Fehlerbehandlung sehr wichtig sein.

(pause: 1)

Alle immutable Objekte sind Thread safe, da sie nicht veränderbar sind. Da niemand das Objekt verändern kann, kann es auch beliebig von verschiedenen Threads verwendet werden ohne dass es in einen invaliden Zustand gerät.

(pause: 1)

Dies gilt nicht nur für Threads sondern auch in anderen Fällen wo Objekte geteilt werden. Häufig verwendete Objekte können zentral bereit gestellt werden. Ein Beispiel ist bei BigDecimal der  Zahlenwert 0, der über eine statische Konstante zur Verfügung gestellt wird.

Immutables sind auch besonders als Keys in Maps geeignet. Da auch dort die Unveränderlichkeit dafür sorgt, dass die Map nicht in einen invaliden Zustand gerät.

(pause: 1)

```md
## Warum?
- immer in einem validen Zustand
- Thread safe
- können beliebig geteilt werde
- geeignet als Keys in Maps
```

---

Schauen wir uns an was passieren kann, wenn man mutable Objekte als Key in einer HäshMäp verwendet.
Wir erstellen eine HäshMäp und speichern dort zu Punkten eine Bemerkung. Die Punkte dienen als Keys.
Das Hinzufügen der Punkte und das anschließende get funktionieren wie erwartet. Nun verändern wir point1 und damit den Key.

(pause: 1)

Wenn wir nun aus der Map den Wert für point1 lesen bekommen wir nall zurück. Da sich die Änderung an point1 auf den hash und den equals Vergleich auswirkt, können wir mit point1 die Anmerkung nicht mehr aus der Mäp lesen.

(pause: 1)


(font-size: 40)
```java
Map<MutablePoint, String> pointsToRemark = new HashMap<>();

var point1 = new MutablePoint(1, 1);
var point2 = new MutablePoint(2, 2);

pointsToRemark.put(point1, "Remark1");

System.out.println(pointsToRemark.get(point1)); // => Remark1

point1.setX(11); //change key

System.out.println(pointsToRemark.get(point1));// => null
```

---

Wie lassen sich nun immutable Objekte erstellen und welche Herausforderungen gibt es dabei?

(pause: 1)

In dem Buch "effective java" von Joschua Bloch wird eine Anleitung mit fünf Schritten vorgestellt.

(pause: 1)

Es dürfen keine Methoden existieren um das Objekt zu verändern.
Man muss sicherstellen, dass die Klasse nicht erweitert werden kann.
Alle Felder müssen [final]{en} und [private]{en} sein.
Die Klasse hat exklusiven Zugriff auf veränderbare Felder.

(pause: 1)

Was bedeutet das nun alles im Detail? Schauen wir es uns gemeinsam an.

```md
## Wie?
1. keine Methoden um Objekt zu verändern (keine mutators)
2. Klasse darf nicht erweiterbar sein
3. alle Felder final machen
4. alle Felder private machen
5. exklusiven Zugriff auf veränderbare Felder
```

---

Der erste Punkt ist recht offensichtlich. Da man erreichen möchte dass das Objekt unveränderlich sein soll, darf man auch keine Methoden zur Verfügung stellen welche eine Veränderung ermöglichen.

```md
## keine Methoden um Objekt zu verändern
```

---

Eine Klasse sollte nicht erweiterbar sein, damit sich Kindklassen weder absichtlich oder versehentlich so verhalten können, als ob das Objekt veränderbar wäre. Man kann das dadurch erreichen, dass die Klasse final ist oder dass jeder Konstruktor private oder package-private sind. Falls man jeden Konstruktor versteckt, kann man statische Factory Methoden bereitstellen um Objekte zu erzeugen.

```md
## Klasse darf nicht erweiterbar sein
```

---

Hier sehen wir ein Beispiel, wie dieser Ansatz aussehen kann. Wenn man den Konstruktor package-private lässt, lassen sich innerhalb des packages weitere Implementierungen erstellen. Außerhalb des packages ist das nicht möglich. Außerdem könnte man in weiteren Releases z.B. ein caching einbauen welches die Factory Methode effizienter macht.

(pause: 1)

```java
private final int x;
private final int y;

private ImmutablePoint(final int x, final int y) {
    this.x = x;
    this.y = y;
}

public static ImmutablePoint of(final int x, final int y) {
        return new ImmutablePoint(x,y);
}
```

---

Dadurch, dass wir alle Felder final machen, zeigen wir was unsere Idee ist, aber wir garantieren auch dass neue Objekte vollständig initialisiert sind. Dies gilt auch bei mehreren Threads, da ein Objekt erst als komplett initialisiert angesehen wird, sobald der Konstruktor vollständig durchlaufen wurde.

```md
## alle Felder final machen

    private final int x;
    private final int y;

```

---

Falls unser Objekt veränderliche Felder enthält, wie zum Beispiel eine Liste, und das Feld nicht [private]{en} wäre könnte dieses Feld ohne weitere Kontrolle modifiziert werden.
Da das unter keinen Umständen passieren darf, sollten alle Felder [private]{en} sein.

(pause: 1)

```md
## alle Felder private machen

    private final int x;
    private final int y;
    private final List<MutablePoint> neighbors;

```

---

Mit exklusiven Zugriff ist gemeint, dass niemand außer das Immutable Objekt selbst Felder bearbeiten darf. Falls man nun eine Referenz auf eine Liste nach außen gibt, kann der Aufrufer Elemente aus der Liste entfernen oder hinzufügen. Dies darf nicht der Fall sein.

(pause: 1)

Um dies zu verhindern kann man zum Beispiel mit einer unmodifiable Liste aus der Collections Klasse arbeiten.

```md
## exklusiven Zugriff auf veränderbare Felder

     public List<MutablePoint> getNeighbors() {
        return Collections.unmodifiableList(neighbors);
    }

```

---

Eine andere Möglichkeit sind "defensive-copies". Man gibt nicht das Objekt direkt heraus sondern eine Kopie. Sonst wäre es möglich von außen den Inhalt zu verändern. Machen wir aber eine Kopie dieses Objektes ist eine Änderung von außen nicht mehr möglich.


```java
public StringBuilder getCommentBuilder() {
    return new StringBuilder(commentBuilder);
}

```

---

Übrigens gilt das gleiche auch für Referenzen die uns im Konstruktor übergeben werden, auch hier müssen wir eine Kopie erstellen um uns von nicht gewollten Änderungen abzuschirmen.


```java
private ImmutablePoint(final int x,
                       final int y,
                       final List<Point> neighbors,
                       final StringBuilder commentBuilder) {
    this.x = x;
    this.y = y;
    this.neighbors = new ArrayList<>(neighbors);
    this.commentBuilder = new StringBuilder(commentBuilder);
}
```

---

Bisher haben wir uns die positiven Seiten von immutable Objekten angeschaut. Aber gibt es auch Nachteile?

(pause: 1)

```md
## Nachteile?
```

---

Der größte Nachteil von immutable Objekten ist, dass für jeden Wert ein neues Objekt erstellt werden muss. Das kann vor allem bei Objekten, die eine aufwendige Initialisierung haben zum Problem werden.

```md
## Nachteile
- pro Wert ein Objekt
```

---

In solchen Fällen wird dann gerne eine "companion class", also ein Kamerad zur Seite gestellt.
Diese Partnerklasse ist veränderbar und kann die Erstellung effizienter machen.
Ein prominentes Beispiel ist der StringBuilder. Der StringBuilder selbst ist mutable, der resultierende String nicht.

```java
final StringBuilder builder = new StringBuilder("build");
builder.append("a");
builder.append("long");
builder.append("string");
final String result = builder.toString();
```

---

Muss man immutable Objekte komplett von Hand schreiben oder kann ich mich unterstützen lassen?

```md
## Alles von Hand?
```

---

Programmierer sind faul und daher gibt es auch hierzu schon Bibliotheken die einem viel Arbeit abnehmen.
Lombok ist hier wieder ein möglicher Kandidat.

```java
@Value
public class ImmutableLombokPoint {

    private final int x;
    private final int y;
    private final List<Point> neighbors;
    private final StringBuilder commentBuilder;

}
```

---

Wer die Möglichkeit hat Java 14 oder neuer einzusetzen, kann auch den Record nutzen.
Leider haben sowohl der Record als auch der Lombok Ansatz Schwächen.
Beide machen keine Kopie der Objekte welche über den Konstruktor übergeben werden und ermöglichen es so Interna des Immutable Objekt zu verändern.


```java
public record PointAsRecord(int x,
                            int y,
                            List<Point> neighbors,
                            StringBuilder commentBuilder) {

}
```

---

 In beiden Fällen lässt sich das Problem beheben, wenn man den Konstruktor selbst implementiert. Wie hier für den Lombok Ansatz:

(font-size: 40)
```java
public ImmutableLombokPoint(final int x,
                                final int y,
                                final List<Point> neighbors,
                                final StringBuilder commentBuilder) {
    this.x = x;
    this.y = y;
    this.neighbors = new ArrayList<>(neighbors);
    this.commentBuilder = new StringBuilder(commentBuilder);
    }
```

---

Alle der vorgestellten Möglichkeiten ein Immutable Objekt zu erzeugen, schützen nicht davor, dass Mutable Objekte welche sich in einer Liste befinden verändert werden können.
Angenommen in der Liste der Nachbarn sind Punkte welche nicht immutable sind. Eine Veränderung an einem Nachbar ändert implizit unser immutable Objekt.

Entweder muss eine deep copy der Liste gemacht werden oder die Liste darf nur Immutable Objekte enthalten.

```md
## Fallstricke

    immutablePoint.getNeighbors().get(0).setX(666);

```

---

weitere Infos und den Code findet ihr hier.
Vielen Dank für eure Zeit und bis bald!

```md
## weitere Infos
- https://bitbucket.org/esentri/coding-essentials/src/master/source/code/
- Buchempfehlung: Effective Java - Joshua Bloch
```

---
